$(document).ready(function($) {

/* ngShowMore - plugin for expanding text */
if($(".ngShowMore").length > 0){

    //first init
    ngShowMoreUpdateMaxHeight();

    //being easy with resize event - debounce code must be included
    window.addEventListener('resize', debounce(function() { ngShowMoreUpdateMaxHeight() }, 500));

    //ngShowMore functionality
    function ngShowMoreUpdateMaxHeight() {
        $(".ngShowMore").each(function(){

            if(!$(this).hasClass("expanded")){
                var ngShowMoreAreaHeight = $(".ngShowMoreArea", this).height();
                var expandedMaxHeight = $(".ngShowMoreInArea", this).height();
                var transitionDuration = parseFloat($(".ngShowMoreArea", this).css("transition-duration"))*1000;

                if ( expandedMaxHeight > ngShowMoreAreaHeight ){

                    $(".ngShowMoreExpander", this).show();
                    $(".ngShowMoreExpander", this).unbind("click"); //clearing posible past events
                    $(".ngShowMoreExpander", this).click(function(e){
                        e.preventDefault();
                        var thisInner = $(this);

                        $(this).siblings(".ngShowMoreArea").css("max-height", expandedMaxHeight);

                        setTimeout(function(){
                            thisInner.siblings(".ngShowMoreArea").css("max-height", "none");
                        }, transitionDuration+250);

                        $(this).hide();
                        $(this).parent().addClass("expanded");
                    });
                }
                else if ( expandedMaxHeight == ngShowMoreAreaHeight ) {
                    $(".ngShowMoreExpander", this).hide();
                }
            }

        });
    }

}
/* /ngShowMore - plugin for expanding text */

});